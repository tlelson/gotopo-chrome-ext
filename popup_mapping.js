// Page variables
var pageMID;

// Background functions
var bgPage = chrome.extension.getBackgroundPage();
var getAuthToken = bgPage.getAuthToken;
var getMapID = bgPage.getMID;

var inprogress;

function startProgress() {
  inprogress = setInterval(function(){ myProgress() }, 800);
}

function myProgress() {
	var d = new Date();
	var t = d.toLocaleTimeString();
	$("#clock").text(t);

  // update progrees
  let new_prog;
  let current_progress = $('#progress').text();
  if (current_progress.length > 9)
    new_prog = '.';
  else
    new_prog = current_progress + '.';
  $('#progress').text(new_prog);
}

function stopProgress() {
    clearInterval(inprogress);
}

function enableFeatures(obj) {
  if (obj.error) {
    console.log('On the MyMaps start page ...');
  } else {
    console.log('MID is: ' + obj.mid);
    // enable Accordion
    $('#mapOptionsDisabled').hide();
    $('#mapOptionsEnabled').show();
    // enable the 'getMapBtn'
    $('#getMapBtn').prop('disabled', false);
    // save to later send to server
    pageMID = obj.mid;
  }
}

function downloadMap(response, textStatus, jqXhr) {
  // param obj: This is the response object from $.ajax()
  // this function should be used as the callback for the .done()

  if (response.download_url){
    custom_map_name = $("input[name='mapTitle']").val();

    if (custom_map_name) {
      params = {
        url: response.download_url,
        saveAs: false,
        filename: custom_map_name + '.jpeg'
      }
    } else {
      params = {
        url: response.download_url,
        saveAs: false
      }
    }
    console.log('Downloading from: ' + response.download_url);
    chrome.downloads.download(params);
  } else {
    msg = "Success without 'download_url'! Please report to developers.";
    doAlert(msg);
  }
}

function getTopo(token) {
  console.log('Requesting Topo ...');
  // Use following to run with curl
  //console.log('URL: ' +  gotopoAPI + 'map/' + pageMID);
  //console.log(token);

  $.ajax({
    headers : {
        'chromeauthtoken' : token.token,
        'Accept' : 'application/json',
        'Content-Type' : 'application/json'
    },
    url : gotopoAPI + 'map/' + pageMID,
    type : 'GET'
  })
  .done(downloadMap)
  .fail(function (jqXhr, textStatus, errorThrown) {
    if (jqXhr.status == 422) {
      msg = 'Map not shared publicly. If you dont own this map, make a copy.'
    } else {
      msg = jqXhr.responseText;
    }
    doAlert(msg);
  })
  .always(function () {
    console.log('Always');
    resetMapBtn();
    getAuthToken(false, setMapCredits);
  });
}

function resetMapBtn() {
  let getMapBtn = $("#getMapBtn")
  stopProgress();
  getMapBtn.text("Generate Topo");
  getMapBtn.prop('disabled', false);
  getMapBtn.data('executing', false);
}

function isReadyToGenerate() {
  let getMapBtn = $('#getMapBtn');

  if (getMapBtn.prop('executing'))
    return false

  if (!pageMID) {
    msg = 'Error: Could not determine Map ID (mid) ...';
    console.log(msg);
    doAlert(msg);
    return false
  }

  // Use this condition so that text fails
  if (!($('#creditNum').text() >= 1)){
    var element_to_hylight = $('#creditNum').closest('.row');
    element_to_hylight.animate({'background-color': 'rgba(255, 0, 0, 0.29)'});
    element_to_hylight.animate({'background-color': 'white'});
    return false
  }
  return true
}

function startDownloadingSteps() {
  $('#confirmGenerate').modal('hide');
  // Dont request if already requesting
  let getMapBtn = $('#getMapBtn');
  getMapBtn.data('executing', true);
  getMapBtn.prop('disabled', true);
  getMapBtn.html("generating<span id='progress'></span>");
  startProgress();
  getAuthToken(false, getTopo);
}

// associated javascript
$(document).ready(function () {
  // Enable all carat toggling
  $('#confirmBtn').click(startDownloadingSteps);

  // Do basic checks before generating map
  $('#getMapBtn').click( function (event) {
    if (isReadyToGenerate()) {
      console.log('Allow modal');
      $('#confirmGenerate').modal('show');
    } else {
      console.log('prevent modal opening');
    }
  });

  getMapID(enableFeatures);
  console.log('mapping: Document ready ...');
})
