// associated javascript

// Background functions
var bgPage = chrome.extension.getBackgroundPage();
var getAuthToken = bgPage.getAuthToken;
var getProfileUserInfoMine = bgPage.getProfileUserInfoMine;
// Background variables
var googleid = bgPage.googleid;
var userEmail = bgPage.userEmail;
var givenName = bgPage.givenName;
var profilePic = bgPage.profilePic;
var gotopoAPI = bgPage.gotopoAPI;
var errorMsg = bgPage.errorMsg;
var clientTokenPromise  = bgPage.braintree_clientId_promise;

// Page variables
var clientToken;
var creditsIsValid = true;

function doAlert(msg, level='warning') {
  console.log(msg);
  let heading = '';
  if (level === 'warning') heading = '<strong>Error</strong>';

  var alerthtml = `
    <div class="alert alert-${level} alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      ${heading}
      <p id="msgDetail">Unknown error </p>
    </div>
  `;
  $('#welcome').after(alerthtml);
  $("#msgDetail").text(msg);
}

function creditValidator() {
  var ctb = $('#creditsToBuy');
  var ctbBtn = $('#getCreditBtn');

  let invalid = (
    (ctb.val() != Math.floor(ctb.val()))
    || !(ctb.val() > 0)
    || (ctb.val() > 100)
  );

  if (invalid){
    // Highlight
    ctb.addClass('form-control-danger');
    ctb.closest('.input-group').addClass('has-danger');
    // Display help text
    creditsIsValid = false;
    ctbBtn.addClass('invalid');
    ctbBtn.val('Whole numbers only!')
  } else {
    // Highlight
    ctb.removeClass('form-control-danger');
    ctb.closest('.input-group').removeClass('has-danger');
    // Display help text
    creditsIsValid = true;
    ctbBtn.removeClass('invalid');
    ctbBtn.val(`Charge $${Number(ctb.val())}`)
  }
  $('#getCreditBtn').prop('disabled', !creditsIsValid);
}

// Functions for AJAX requests
function get_credit_headers(tok) {
  return {
    'chromeauthtoken' : tok,
    'Accept' : 'application/json',
    'Content-Type' : 'application/json'
  };
}

function credit_success(response, textStatus, jqXhr) {
  console.log('response: ' + response.map_credits);
  console.log('textStatus: ' + textStatus);
  console.log('jqXhr: ' + jqXhr);

  var mc;

  if (response.message) doAlert(response.message, level='success');
  if (response.error_message) doAlert(response.error_message, level='warning');

  if (response.hasOwnProperty('map_credits')) {
    mc = (response.map_credits > 0) ? response.map_credits : 0;
  } else {
    mc = 'operation failed';
  }
  $('#creditNum').text(mc);
}

function credit_failed(jqXhr, textStatus, errorThrown) {
  console.log('errorThrown: ' + errorThrown);
  console.log('textStatus: ' + textStatus);
  console.log('jqXhr: ' + jqXhr);

  let msg = jqXhr.responseText;
  if (!msg)
    msg = 'Network error. Check your connection and reopen chrome';
  else
    msg = jqXhr.responseText.split(':')[1];

  // Catch special error cases
  const nonce_used_regex = /Cannot use a payment_method_nonce more than once/g;
  var match = nonce_used_regex.exec(msg);
  if (match)
    msg = 'Refresh page to buy more credit'

  doAlert(msg, level='warning');
}

function end_getting_credit() {
  // Re-enable credit UI
  console.log('Finished trying to get credit');
  let creditBtns = $('#creditForm').find('.btn');
  creditBtns.data('executing', false);
  creditBtns.prop('disabled', false);
}

function start_getting_credit() {
  // Set up UI to look like its thinking

  console.log('Trying to get credit');
  let creditBtns = $('#creditForm').find('.btn');
  creditBtns.data('executing', true);
  creditBtns.prop('disabled', true);
}

// AJAX functions
function setMapCredits(token) {
  console.log('Getting users available map credits ...');
  var url = gotopoAPI + "credit" ;

  $.ajax({
    headers : get_credit_headers(token.token),
    url : url,
    type : 'GET',
  })
  .done(credit_success)
  .fail(credit_failed)
  .always(function () {
    console.log('Always');
  });
}

function deleteCreditUser(token) {
  console.log('Deleting the user from GoTopo Database ...');
  var url = gotopoAPI + "credit" ;

  $.ajax({
    headers : get_credit_headers(token.token),
    url : url,
    type : 'DELETE',
  })
  .done(credit_success)
  .fail(credit_failed)
  .always(function () {
    console.log('Always');
  });
}

function getCouponCredit(token) {
  console.log('Trying coupon code ...');

  // Disable credit buttons etc
  start_getting_credit();

  let coupCode = $('#couponCode').val()

  // Validate code first

  var url = gotopoAPI + "credit/" + coupCode ;

  $.ajax({
    headers : get_credit_headers(token.token),
    url : url,
    type : 'GET',
  })
  .done(credit_success)
  .fail(credit_failed)
  .always(end_getting_credit);
}

function buyMapCredits(token) {
  /* N.B that sometimes there are multiple payment_method_nonce's
   * As long as the backend takes the last one in the list the
   * user will be changed to the method they selected
   * */
  console.log('buyMapCredits reached ...');

  // Disable credit buttons etc
  start_getting_credit();

  console.log( $('#checkout').serialize() );
  console.log( $('#checkout').serializeArray() );

  /* Clean form data
  !! This creates an array of values if there is more than one field
  with the same name.  */
  var form_data = $('#checkout').serializeArray() ;
  var o = {};
  $.each(form_data, function() {
    if (o[this.name] !== undefined) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });

  console.log('Sending following data to my endpoint');
  console.log(o);
  console.log('Buying users available map credits ...');
  var url = gotopoAPI + "addcredit" ;

  $.ajax({
    headers : get_credit_headers(token.token),
    url : url,
    data: JSON.stringify(o),
    //dataType: 'application/json',
    type : 'POST'
  })
  .done(function (response, textStatus, jqXhr) {
    credit_success(response, textStatus, jqXhr);
    $('#creditForm').collapse('hide');
  })
  .fail(credit_failed)
  .always(end_getting_credit);
}

$(document).ready(function () {
  // Add Event Listeners
  // 1. Set up links/buttons
  $('footer').on('click', 'a', function () {
    console.log("how to linked clicked ...");
    chrome.tabs.create({'url': 'http://gotopo.co'});
  });
  $('#gmailAddress').on('click', function () {
    console.log("Map options clicked ...");
    chrome.tabs.create({'url': 'chrome://settings/'})
  });
  $('#useCouponBtn').on('click', function () {
    if ($(this).data('executing')) return;

    getAuthToken(false, function (token) {
      $('#creditNum').text('requesting ...');
      getCouponCredit(token);
    });
  });
  $('#creditsToBuy').on('input', creditValidator);

  //2. Enable carat switching for collapsibles
  $('.panel-collapse').on('hide.bs.collapse', function () {
    console.log('A collapsible was collapsed');
    var triangle = $(this).closest('.panel-default').find('i.fa');
    triangle.toggleClass('fa-caret-right');
    triangle.toggleClass('fa-caret-down');
  });
  $('.panel-collapse').on('show.bs.collapse', function () {
    console.log('A collapsible was opened');
    var triangle = $(this).closest('.panel-default').find('i.fa');
    triangle.toggleClass('fa-caret-right');
    triangle.toggleClass('fa-caret-down');
  });


  //3. Personalise for user
  $('#gmailAddress').text(userEmail);
  $('#userName').text(givenName + ' !');
  // If the popup is the error page ...
  $('#errorDetail').text(errorMsg);

  //4. Set credit spend value
  var ctb = $('#creditsToBuy');
  $('#getCreditBtn').val(`Charge $${ctb.val()}`)
  console.log('common: Document ready ...');
});

$(window).load(function (){
  /* Do the long job here.  Opening the popup can take along time if
   * this is dont in $(document).ready
   * */

  // Get their current map credits
  getAuthToken(false, setMapCredits);

  // Code for braintree checkout dropin
  // dont set it up until the client token is received
  clientTokenPromise
  .then(function () {
    clientToken = bgPage.braintree_clientId;
    if (!clientToken) {
      console.log('Braintree ClientID fetch failed');
      return;
    }
    braintree.setup(clientToken, "dropin", {
      container: "payment-form",
      paymentMethodNonceReceived: function (event, nonce) {
        if ($('#getCreditBtn').data('executing')) return;
        $('#checkout').append("<input type='hidden' name='payment_method_nonce' value='" + nonce + "'></input>");
        getAuthToken(false, buyMapCredits);
      }
    });
  })
  .catch(function () {
    console.log('unreachable error ... ');
    // Unreachable because the promise has a catch
    // already on it in the background.js.
    // The way an error is detected is by checking
    // if the clientToken is null
  });
  console.log('Window loaded ...');
});
