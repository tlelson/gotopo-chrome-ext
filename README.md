# GoTopo Chrome Extension

*this 'after-work' project should never have become so elaborate but it became more useful than I though*

## Aim:
Learn how to make Chrome extensions while improving javascript/front-end skills

## Result:
A quick way to produce maps suitable for compass navigation (Rogaining, Climbing etc) using Google MyMaps.

## How it works:
The chrome extension sends the Google MyMaps, MapID to an API backed by Python code hosted in AWS lambda.  The API builds the map and returns the image.

![usage image](usage.png)

[Chrome Extension](https://chrome.google.com/webstore/detail/gotopo-plugin/ijgmjcniihcddkionmcjjihbmlaljbco)

# Development

## Branch: master
This is the branch that is pubished to chrome webstore

## Branch: dev
This is where dev work is being done ...

## About
Bower is used to manage the components.

Install all package dependencies with:
```bash
$ bower install
```

Install new packages with:
```bash
$ bower install <package> -S
```
## Publish to Webstore

- Merge `dev` -> `master`
- **Dont tag yet**

Ensure:

- APIADDR in `background.js` points to prod
- `manifest.json` is ready for upload
- The version number is augmented in `manifest.json`

Finally:

- zip up the entire directory
```bash
$ zip -x *.git/* -r gotopo_ext.zip gotopo-chrome-ext
```

- Go to 'theboss@gotopo.co' Webstore account and upload the zip

Finally (when upload is successful NOT BEFORE):
- commit all changes to `master`
- tag with the version number

