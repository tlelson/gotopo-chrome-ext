// CONSTANTS
// I know this is terrible and opens DNS spoofing attack vectors
// but i dont want to:
//    - have to hardcode the API Gateway cloudfront addr
//    - thus republish this exenstion every time the API changes
// I also dont want fix it all the right way because:
//    - Cloudfront only allows https
//    - I dont want to buy SSL certs
//    http://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-custom-domains.html
const APIADDR  = "http://gotopo.co/apiaddr-prod.txt";
//const APIADDR = "http://gotopo.co/apiaddr-dev.txt";

// GLOBALS
var gotopoAPI = null;
var errorMsg = null;
var googleid, userEmail, givenName, profilePic, braintree_clientId, braintree_clientId_promise;

// FUNCTIONS
var manifest = chrome.runtime.getManifest();
var scopes = manifest.oauth2.scopes;
var popups = chrome.extension.getViews({type: "popup"});

function getPaymentClientId() {
  getAuthToken(false, function (res) {
    if (res.error) return;

    let headers = new Headers({
      'Content-Type': 'application/json',
      'chromeauthtoken': res.token,
    });
    let url = gotopoAPI + 'clientid';
    let request = new Request(url, {headers});

    console.log('Requesting a braintree clientID');

    braintree_clientId_promise = fetch(request)
    .then( function (res) {
      return res.text();
    })
    .then( function (responseText) {
      console.log('Setting braintree_clientId');
      braintree_clientId = responseText;
    })
    .catch( function () {
      console.log('Error getting braintree_clientId');
      braintree_clientId = null;
    });
  });
}

function getCurrentTabUrl(callback) {
  // param callback: functions takes single parameter 'url'
  // eg:  function(url){ ...

  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    var tab = tabs[0];
    var url;
    try { // if inside the debugger
      url = tab.url;
    } catch (e) {
      console.log('Exception caught: ' + e.message);
      url = 'notabURL';
    }

    console.assert(typeof url == 'string', 'tab.url should be a string');
    callback(url);
  });
}

function getAuthToken(interactive, callback) {
  // Its ok to do this whenever because Chrome handles caching
  // param interactive: boolean. if true the user will be asked to
  // signin if no token is found in the cache
  // param callback: function with signature function({token, error})
  // check for 'error' first
  // e.g: getAuthToken(false, getProfileUserInfo);
  // return object: {token, error}
  // If token.token is null -> Chrome failed to get the users ID

  var details = {
    'interactive': interactive,
    "scopes": scopes
  }

  chrome.identity.getAuthToken(details, function (token) {
    var err = chrome.runtime.lastError;
    if (err) {
      console.log('getAuthToken Error: ' + err.message);
      callback({'token': null, 'error': err.message});
    } else {
      console.log("Token: " + token);
      callback({token});
    }
  });
}

function getInfoFromToken(type, token, callback) {
  /* param type: 'user' or 'token'. User info returns email address etc.  Token
   * info returns qualities of the actual token, such as its expiry time.
   * param token: Chrome auth token
   */

  if (type === 'user')
    var url = "https://www.googleapis.com/oauth2/v3/userinfo?access_token=";
  else if ( type === 'token')
    var url = "https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=";
  else
    throw new Error('Invalid type')

  fetch( url + token )
  .then( res => res.json() )
  .then( data => callback(data) )
  .catch( err => console.log(`Failed to get ${type}info: ` + err) )
}

function onMapPage(url) {
  // Get started page
  const my_maps_url = 'https://www.google.com/maps/d/';

  // Regex's
  const my_maps_re = /(^https:\/\/www\.google\..*\/maps\/d\/)/g;

  // Do a match
  var match = my_maps_re.exec(url);

  // 3. Get the Map ID
  var isMyMapPage = false;
  if (!match){
    console.log("Not on a MyMaps page ...");
    return false;
  }
  else {
    console.log("On a MyMaps page ...");
    return true
  }
}

function setUserDetails(obj) {
  console.log('getting Users Profile info: ');
  googleid = obj.sub;
  userEmail = obj.email;
  givenName = obj.given_name;
  profilePic = obj.picture;
  console.log('Finished setting user data ...');
}

function setAPIurl(callback) {
  /* This has to catch 3 possible failures:
   *    - Can't download apiaddr.txt file: "<URL>: <statuscode>"
   *    - Cant get response from '/ping': "Failed to fetch"
   *    - Gets wrong response from '/ping:" Response not 'pong'"
   *
   * All must set an error message and call the callback.
   * Otherwise the popup will not be set to error and the
   * use will be told they are not loged in.  Frustrating.
   * */

  let headers = new Headers({
    'pragma': 'no-cache',
    'cache-control': 'no-cache',
  });

  fetch(APIADDR, {headers})
  .then( res => {
    if (!res.ok) throw new Error(res.url + ": " + res.statusText);
    return res.text()
  })
  .then( responseText => {
    console.log(responseText);
    gotopoAPI = responseText })
  .then( () => {
    //Needs to return something for the promise
    return fetch(gotopoAPI + 'ping', {headers})
    .then( res => {
      if (!res.ok) throw new Error(res.url + ": " + res.statusText);
      return res.text()
    })
    .then(responseText => {
      if (responseText.trim() !== 'pong')
        throw new Error("Response not 'pong' ");
      callback()
    })
    .catch(err => { throw new Error(err) });
  })
  .catch(function (err) {
    errorMsg = 'Unable to contact gotopo';
    gotopoAPI = null;  // reset so tries again next time
    console.log(errorMsg);
    callback(); // need to call this so that error popup can be set
  });
}

function setPopup() {
  // Sets the popup.html based on the current page
  // and user login status

  if (errorMsg){
    console.log('Error message detected. Displaying');
    chrome.browserAction.setPopup({'popup': 'popup_error.html'});
  } else {
    // first check user is loged in
    getAuthToken(false, function (res) {
      if (res.error)
        console.log('User not logged into Chrome: Holding popup_not_signedin.html');
      else { // User is logged in ...
        if (!userEmail)
          getInfoFromToken('user', res.token, setUserDetails);

        console.log('Checking current tab to set relevent popup...');
        getCurrentTabUrl(function (url) {
          console.log('Current tab URL: ' + url);
          if (onMapPage(url)) {
            console.log('Display popup_mapping.html');
            chrome.browserAction.setPopup({'popup': 'popup_mapping.html'});
          } else {
            console.log("Display popup_wrong_page.html");
            chrome.browserAction.setPopup({'popup': 'popup_wrong_page.html'});
          }
        });
      }
    });
  }
}

function display_cookies() {
  chrome.cookies.getAll({}, function (arr) {
    console.log(arr);
  });
}

function runOnPageChange(){
  // Reset the message to allow error to be resolved
  errorMsg = null;

  if (!gotopoAPI){
    setAPIurl(function () {
      setPopup();
      getPaymentClientId();  // Only do this once
    });
  } else {
    setPopup();
  }
}

// Work Flow
// 1. Add listener for tab change events
// (may be able to turn these off with event pages)
chrome.tabs.onActivated.addListener(function (activeInfo) {
  runOnPageChange();
});
chrome.tabs.onUpdated.addListener(function (tabID, changeInfo, tab) {
  if (changeInfo.status !== 'complete')
    return
  runOnPageChange();
});
chrome.windows.onFocusChanged.addListener(function (windowID) {
  console.log('** onFocusChanged');
  if (windowID > 0) {
    console.log('New window focused: ' + windowID);
    runOnPageChange();
  } else {
    console.log('Chrome window UNfocused (-1): ' + windowID);
  }
})

// More restrictive eventListener: webNavigation.onCompleted
// is no good because it runs ~3 times for some reason ... sticking with the old method
//var filterParams = { url: [ {hostContains: 'google.com'} ] };
//chrome.webNavigation.onCompleted.addListener(setPopup, filterParams);

// 2. Run once when the extension starts
runOnPageChange();

// This function is called by the popup_*.js files
function getMID(callback) {
  getCurrentTabUrl(function (url) {
    console.log("gettin MapID from: " + url);

    var mid = null;

    // Do a match
    const active_map_re = /(^https:\/\/www\.google\..*\/maps\/d\/).*\?.*mid=([-a-zA-Z0-9@:%_\+.~#?&=]{28})/g;
    var match = active_map_re.exec(url);

    if (!match) {
      console.log("no MID found ...");
      callback({'error': 'no MID found'})
    } else {
      mid = match[2];
      console.log("Map ID: " + mid);
      callback({mid});
    }
  });
}

