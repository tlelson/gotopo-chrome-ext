var bgPage = chrome.extension.getBackgroundPage();
var getAuthToken = bgPage.getAuthToken;

// associated javascript
$(document).ready(function () {
  console.log('document ready ...');
  $('#chromeSettings').on('click', function () {
    console.log("Link to chrome settings clicked ...");
    chrome.tabs.create({'url': 'chrome://settings/'})
  });
  $('#signIn').on('click', function () {
    console.log("Signin into chrome ...");
    getAuthToken(true, function () {
      console.log('going to get auth token ...');
      // TODO: Open a new tab ... to force the background.js to run 'setPopup' again
    });
  });
})

